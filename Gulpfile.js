var gulp = require('gulp');
var browserify = require('browserify');
var babelify = require('babelify');
var source = require('vinyl-source-stream');


let _source = "source";
let _out = "allInOne.js";
let _dist = "./app/build/";

gulp.task('default', function () {
  return  browserify('./app/source/app.js')
    .transform(babelify)
    .bundle()
    .pipe(source(_out))
    .pipe(gulp.dest(_dist));
});
