https://tylermcginnis.com/reactjs-tutorial-a-comprehensive-guide-to-building-apps-with-react/
https://medium.freecodecamp.com/javascript-modules-a-beginner-s-guide-783f7d7a5fcc#.g9p44q9ly
https://medium.freecodecamp.com/javascript-modules-part-2-module-bundling-5020383cf306

Webpack
https://www.codementor.io/tamizhvendan/beginner-guide-setup-reactjs-environment-npm-babel-6-webpack-du107r9zr
https://medium.com/@fay_jai/getting-started-with-reactjs-typescript-and-webpack-95dcaa0ed33c

gulp-webpack-react-babel: 
http://www.knowstack.com/react-and-es6-with-babel-and-webpack/
(https://github.com/nishantnisonko/react-es6)

https://stormpath.com/blog/optimize-react-webpack


https://github.com/jacobmarshall-etc/gulp-webpack-react-babel

Virtual DOM : 
https://www.packtpub.com/mapt/book/web-development/9781783551620/2/ch02lvl1sec19/Understanding+the+virtual+DOM

https://zapier.com/engineering/react-js-tutorial-guide-gotchas/

https://www.sitepoint.com/getting-started-react-jsx/
https://medium.com/@ericclemmons/javascript-fatigue-48d4011b6fc4#.n6vom88ha
https://medium.com/@_erikaybar/one-developers-condensed-intro-to-react-68cbf078f992#.3k1yvpmgz
https://zapier.com/engineering/react-js-tutorial-guide-gotchas/


JSX with Gulp 
https://jonsuh.com/blog/integrating-react-with-gulp/
https://gist.github.com/gradosevic/97bcde437502d021c6e1
http://www.bebetterdeveloper.com/coding/es6-react-babel.html


Search : 
https://www.quora.com/Where-can-I-find-the-best-React-js-tutorial-PDF

Angular4:
https://github.com/AngularClass/angular-examples

react - ts
http://blog.wolksoftware.com/working-with-react-and-typescript

npm notes - 
babel-core is babel itself, babel-loader is the webpack loader we’ll use, and babel-preset-react is to get the JSX -> JS transformation going.