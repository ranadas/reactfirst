// This is the React application 
var React = require('react');
var ReactDOM = require('react-dom');
var utils = require ('./utils/utils');

ReactDOM.render(
  React.createElement('h1', null, 'Hello World!'),
  document.getElementById('app2')
);

//React.createElement(elementNameOrClass, props, children...)
var reactH2Element = React.createElement('h2', {}, 'Hello World!!');
ReactDOM.render(reactH2Element, document.getElementById('react-app'));
