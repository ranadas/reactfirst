var path                = require('path');
var HtmlWebpackPlugin   = require('html-webpack-plugin')
var HTMLWebpackPluginConfig = new HtmlWebpackPlugin({
            template: __dirname + '/app/index.html',
            filename: 'index.html',
            inject: 'body'
});

var APP_DIR = path.resolve(__dirname, 'app/source/');
var BUILD_DIR = path.resolve(__dirname, 'build/');

// In webpack.config.js
module.exports = {
    entry: [
        //Webpack allows to have one or many entry points.
        APP_DIR+'/app.js'
    ],
    // loader needs 3 things. 1. which file type to run transformation on. 2. which directories should be included or excluded.  3. The last thing is the specific loader we want to run.
    module: {
        loaders: [
            { test: /\.jsx?/, include: APP_DIR, loader: 'babel-loader' }
        ]
    },
    output: {                       // output folder
        path: BUILD_DIR,            // folder path
        filename: 'bundled-app.js'  // file name
    },
    plugins: [HTMLWebpackPluginConfig]
};


